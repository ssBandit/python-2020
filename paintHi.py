import pyautogui as pg
import time

pg.hotkey('win', 'r')
pg.typewrite('mspaint')
pg.press('enter')

time.sleep(2)
pg.PAUSE = 0.001
pg.mouseDown()
pg.moveRel(0, 200)
pg.mouseUp()
pg.mouseDown()
pg.moveRel(0, -100)
pg.mouseUp()
pg.mouseDown()
pg.moveRel(100, 0)
pg.mouseUp()
pg.mouseDown()
pg.moveRel(0, -100)
pg.mouseUp()
pg.mouseDown()
pg.moveRel(0, 200)
pg.mouseUp()