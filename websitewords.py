import requests
from bs4 import BeautifulSoup
import wordcloud
from PIL import Image
import numpy

print("Getting website")

site = requests.get("https://en.wikipedia.org/wiki/Python_(programming_language)").content
soup = BeautifulSoup(site, 'html.parser')

allText = ""
for elm in soup.select('div p'):
    allText += " " + elm.text

ignoreWords = wordcloud.STOPWORDS
ignoreWords.add("language")

print("Soup done")

mask = numpy.array(Image.open("mask.png"))

cloud = wordcloud.WordCloud(background_color='white',
                            include_numbers=True,
                            mask=mask,
                            max_words=1000,
                            stopwords=ignoreWords,
                            font_step=3).generate(allText)

cloud.to_file("wordcloud.png")
print("Cloud done")