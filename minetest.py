from tkinter import *
import random

window = Tk()
window.geometry("400x450")

buttons = []
field = []


def OnButtonPress(x, y):
    if(field[x][y] == 0):
        buttons[x][y].config(bg='green')
        field[x][y] = -2
        CheckNeighbors(x,y)
    elif field[x][y] == -1:
        buttons[x][y].config(bg='red')
    elif field[x][y] != -2:
        buttons[x][y].config(text = field[x][y])


def GenerateButtons():
    for x in range(20):
        buttons.append([])
        for y in range(20):
            button = Button(window, width=4, height = 2, command=lambda x=x, y=y: OnButtonPress(x, y))
            button.grid(column = x, row = y)
            buttons[x].append(button)


def GenerateField():
    for x in range(20):
        field.append([])
        for y in range(20):
            field[x].append(0)
            if random.randint(0, 100) < 10:
                field[x][y] = -1
    for x in range(20):
        for y in range(20):
            if(field[x][y] == -1):
                AddToNeighbors(x,y)

def CheckNeighbors(x, y):
    if(x > 0):
        if(y > 0):
            if(field[x-1][y-1] >= 0):
                buttons[x-1][y-1].invoke()
        if(field[x-1][y] > 0):
            buttons[x-1][y].invoke()
        if(y < 19):
            if(field[x-1][y+1] >= 0):
                buttons[x-1][y+1].invoke()

    if(y > 0):
        if(field[x][y-1] >= 0):
            buttons[x][y-1].invoke()
    if(y < 19):
        if(field[x][y+1] >= 0):
            buttons[x][y+1].invoke()
    if(x < 19):
        if(y > 0):
            if(field[x+1][y-1] >= 0):
                buttons[x+1][y-1].invoke()
        if(field[x+1][y] >= 0):
            buttons[x+1][y].invoke()
        if(y < 19):
            if(field[x+1][y+1] >= 0):
                buttons[x+1][y+1].invoke()

def AddToNeighbors(x,y):
    if(x > 0):
        if(y > 0):
            if field[x - 1][y - 1] != -1:
                field[x-1][y-1] += 1
        if field[x - 1][y] != -1:
            field[x-1][y] += 1
        if(y < 9):
            if field[x - 1][y + 1] != -1:
                field[x-1][y + 1] += 1
    if (y > 0):
        if field[x][y - 1] != -1:
            field[x][y - 1] += 1
    if (y < 9):
        if field[x][y + 1] != -1:
            field[x][y + 1] += 1
    if (x < 9):
        if (y > 0):
            if field[x + 1][y - 1] != -1:
                field[x+1][y-1] += 1
        if field[x + 1][y] != -1:
            field[x+1][y] += 1
        if (y < 9):
            if field[x + 1][y + 1] != -1:
                field[x+1][y + 1] += 1

GenerateButtons()
GenerateField()

window.mainloop()