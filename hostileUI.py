from tkinter import *
from tkinter import ttk
import random

#Переменные для ширины и высоты
WIDTH = 500
HEIGHT = 500

#Функция чтобы в рандомное место передвинуть кнопку
def moveButt(data):
    butt.place(x = random.randint(0, WIDTH - 10), y = random.randint(0, HEIGHT - 20))

#Функция чтобы поменять цвет фона окна
def changeColor():
    window.config(bg="lime")

#Создать окно и задать размеры на те которые выше
window = Tk()
window.geometry(str(WIDTH) + "x" + str(HEIGHT))

#Создать кнопку
butt = Button(text="CLICK ME!", width = 10, height = 4, bg='red', command=changeColor)

#Подключить эвент наведения мыши к функции передвинуть кнопку
butt.bind('<Enter>', moveButt)

#Поставить кнопку по центру (более-менее)
butt.place(x = WIDTH/2, y = HEIGHT/2)


window.mainloop()