import requests
from PIL import Image
import os
import math

#Ключь для API
KEY = "9dRXsWAvFsOsJD7fSGmwAncx4FMRKGEYdmtlemMZ"
date = "2021-02-10" #YYYY-MM-DD
#С какой камеры получать фото
cam = "MAST" #FHAZ

#Загрузить картинки
def downloadPics():
    #Получить ответ с ссылки передав дату ключь и камеру
    response = requests.get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date={}&api_key={}&camera={}".format(date, KEY, cam))

    #Считать это как json
    jsonData = response.json()

    #Список ссылок для картинок
    picUrls = []

    #Пройтись по каждой фото и сохранить её ссылку в список
    for photo in jsonData["photos"]:

        photoLink = photo["img_src"]
        print(photoLink)
        #Если картинка маленького размера, то пропустить элемент
        if(photoLink[-12] == "I"):
            continue

        picUrls.append(photoLink)

    c = 0
    #Пройтись по всем ссылкам картинок, получить от них результат и записать в файл
    for URL in picUrls:
        picData = requests.get(URL).content
        #добавить в имя цифру
        file = open("marsPics/img{}.png".format(c), 'wb')
        file.write(picData)
        file.close()

        c += 1

#Соеденить картинки в коллаж
def composite():

    #Заполнить список картинок файлами из папки
    pics = []
    for file in os.listdir("marsPics"):
        pics.append(Image.open("marsPics/" + file))

    #Вычеслить квадратный корень из длинны списка (для размеров коллажа)
    root = math.sqrt(len(pics))
    #Округлить вверх
    root = int(math.ceil(root))

    #Вычеслить ширину и высоту коллажа
    width = pics[0].width * root
    height = pics[0].height * root

    #Создать картинку с такими размерами
    collage = Image.new("RGB", (width, height))

    #Вставить картинки в коллаж
    counter = 0
    for y in range(root):
        for x in range(root):
            if(counter >= len(pics)):
                break
            collage.paste(pics[counter], (pics[0].width * x, pics[0].height * y))
            counter += 1

    collage.save("collage.png")
    collage.show()

downloadPics()
composite()
print("Done")