import random

def checkWin(cpu, player):
    print("Player picked " + player + " computer picked " + cpu)
    if(cpu == player):
        return "Draw! You both lose"
    if(cpu == "R"):
        if(player == "P"):
            return "Player Wins"
        else:
            return "Computer Wins"
    if(cpu == "P"):
        if(player == "S"):
            return "Player Wins"
        else:
            return "Computer Wins"
    if(cpu == "S"):
        if(player == "R"):
            return "Player Wins"
        else:
            return "Computer Wins"

hands = ["R", "P", "S"]

while(True):
    cpuChoice = random.choice(hands)

    #TODO: Remove later
    #print(cpuChoice)

    userChoice = ""

    while(userChoice != "R" and userChoice != "P" and userChoice != "S"):
        userChoice = input("Pick a hand [R][P][S]\n")
        userChoice = userChoice.upper()

    print(checkWin(cpuChoice, userChoice))

