import turtle
import math
import random

turtle.bgcolor("black")

colors = ["red", "orange", "yellow", "green", "blue", "purple"]

print(random.randrange(1 * 100,10 * 100)/100)

#settings
snickers = turtle.Turtle()
snickers.shape("turtle")
snickers.color((0,1,1))
snickers.pensize(3)
snickers.speed(0)
snickers.turtlesize(1)

georg = turtle.Turtle()
georg.shape("turtle")
georg.color((0,1,1))
#movement
def abstract(turt):
    for i in range(50):
        turt.circle(10 + i, steps=3)
        turt.right(170)
        turt.color(colors[i%len(colors)])

snickers.penup()
snickers.goto(200,200)
snickers.pendown()
#abstract(snickers)
#abstract(georg)
abstract(georg)

snickers.forward(200)
snickers.right(120)
snickers.forward(100)
snickers.setheading(90)
snickers.backward(75)

snickers.color("blue")
snickers.penup()
snickers.goto(20, 20)
snickers.pendown()
snickers.forward(90)

snickers.circle(100, steps=6)

turtle.done()