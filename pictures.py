from PIL import Image
import colorsys

#Создание новой картинки размерами 1680 на 1050
#picture = Image.new('RGB', (1680, 1050))

#Окрываем картинку
picture = Image.open("boat.jpg")

#Загружаем пиксели картинки в память чтобы с ними работать на прямую
pixels = picture.load()

#Проходим по каждому пикселю картинки
for y in range(picture.height):
    for x in range(picture.width):
        #Вычесление процента позиции
        px = (1 / picture.width)*x
        py = (1 / picture.width)*y

        #Разделение пикселей в переменные (для удобства) (можно еще кароче - r,g,b = pixels[x,y])
        r = pixels[x,y][0]
        g = pixels[x,y][1]
        b = pixels[x,y][2]

        #Интересный психоделический эффект
        #r = (r + 1) % 200
        #g = (g + 1) % 200
        #b = (b + 1) % 200

        #Вычесление среднего для черно-белого
        avg = (r + g + b) / 3

        #конвертирование черно-белого пикселя в отттенок (для радужного эффекта)
        newColor = colorsys.hsv_to_rgb((avg / 255), 1, 1)

        #r = g = b = avg

        r,g,b = newColor

        #Значения сохранились от 0 до 1 такчто их нужно обратно умножить на 255
        r = r * 255
        g = g * 255
        b = b * 255

        #Задать пикссели картинки
        pixels[x,y] = (int(r), int(g), int(b))

#Сохранить картинку в файл
picture.save("test.png")

#Открыть картинку
#picture.show()