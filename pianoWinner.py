#Бот для игры http://tanksw.com/piano-tiles/
import pyautogui as pg

#Цвет ноты
noteColor = (17,17,17)
#Сколько отступать сверху для проверки на белый
topOffset = 50
#Сколько отступать от центра чтобы не натыкатся на буквы
centerOffset = 30

#Позиции пикселей которые проверять
point1 = (800, 630)
point2 = (900, 630)
point3 = (1000, 630)
point4 = (1100, 630)

#Позиции в списке для удобства
points = [point1, point2, point3, point4]

pg.alert("Starting")

pg.PAUSE = 0.00001

#Бесконечно проверять все точки и если их цвет совподает с цветом ноты, и они не на самом краю ноты то кликнуть на эту точку
while(True):
    for point in points:
        if(pg.pixelMatchesColor(point[0] + 30, point[1] - int(topOffset/2), noteColor)
                and not pg.pixelMatchesColor(point[0] + centerOffset, point[1] - topOffset, (255, 255, 255))):
            pg.click(point)


'''while(True):
    print(pg.position())
    #print(pg.pixel(pg.position()[0], pg.position()[1]))
'''