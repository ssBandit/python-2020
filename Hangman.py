import random
#Рандомно выбрать одно слово
secretWord = random.choice(["glasses", "typewriter", "football", "line", "monitor"])

#Сделать список из знаков "_" в таком количестве какова длинна загаданново слова
letters = ["_"] * len(secretWord)

#количество попыток
lives = 5

#рисует висельницу в зависимости от оставшихся попыток
def drawHangman():
  if(lives == 4):
    print("Г--------\n"\
          "|        \n"\
          "|        \n"\
          "|        \n"\
          "|        \n")
  elif(lives == 3):
    print("Г--------\n"\
          "|    O   \n"\
          "|        \n"\
          "|        \n"\
          "|        \n")
  elif(lives == 2):
    print("Г--------\n"\
          "|    O   \n"\
          "|    |   \n"\
          "|        \n"\
          "|        \n")
  elif(lives == 1):
    print("Г--------\n"\
          "|    O   \n"\
          "|   /|\  \n"\
          "|        \n"\
          "|        \n")
  elif(lives == 0):
    print("Г--------\n"\
          "|    O   \n"\
          "|   /|\  \n"\
          "|   / \  \n"\
          "|        \n")

#пока в результате есть знаки "_" то циклить
while("_" in letters):
  #отрисовать висельницу
  drawHangman()
  #соеденить список пустых букв в один текст
  print(" ".join(letters))

  #спросить у игрока букву
  guess = input("Guess a letter\n")

  #если она есть в секретном слове и длинна догадки 1
  if(guess in secretWord and len(guess) == 1):
    print('correct')

    startPos = 0

	#try-except пробует выполнить код и при ошибке продолжает спрашивать буквы
    try:
	  #разкрыть все одинаковые буквы
      while(startPos < len(secretWord)):
        #получить позицию буквы
        pos = secretWord.index(guess, startPos)
        letters[pos] = guess
        startPos = pos + 1
    except ValueError:
      continue


  #если угадал не правильно то отнять одну попытку
  else:
    print('wrong')
    lives -= 1
    print(str(lives) + " attempts remaining")
	#есщи попыток не осталось то проиграли - выйдти из программы
    if(lives == 0):
      drawHangman()
      input("YOU LOSE")
      exit()


print("YOU WIN!")
