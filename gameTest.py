import pygame

#запустить системы pygame 
pygame.init()

#Создать окно
screen = pygame.display.set_mode( (500,500) )
#Создать часы (для фпс)
clock = pygame.time.Clock()
#Сделать мышь невидимой
pygame.mouse.set_visible(False)

#размеры и скорость круга
circleX = 250
circleY = 250
circleSpeed = 10

#загрузить картинку
bgimage = pygame.image.load("boat.jpg")

#Основной игровой цикл
playing = True
while playing:
	#Лимитировать до 60 FPS
    clock.tick(60)

    #Events
    for event in pygame.event.get():
		#Если нажали закрыть то выйти из цикла
        if event.type == pygame.QUIT:
            playing = False

    #Logic
	#Получить нажатые кнопки
    buttons = pygame.key.get_pressed()
	#Если кнопка была нажата то двигатся в это направление
    if buttons[pygame.K_d]:
        circleX += circleSpeed
    if buttons[pygame.K_a]:
        circleX -= circleSpeed
    if buttons[pygame.K_w]:
        circleY -= circleSpeed
    if buttons[pygame.K_s]:
        circleY += circleSpeed

	#получить позицию мыши
    mousePos = pygame.mouse.get_pos()

    #Draw
	#Заполнить экран черным
    screen.fill( color=(0,0,0) )
	#отрисовать на фоне картинку
    screen.blit(bgimage, (0,0))
	#Нарисовать круг 
    pygame.draw.circle(screen, (0, 255, 0), center = (circleX, circleY), radius = 20 )
	#Нарисовать вророй круг на позиции мыши
    pygame.draw.circle(screen, (255, 0, 0), center = mousePos, radius = 20 )
	
	#Обновить экран
    pygame.display.flip()

print("Goodbye")