from PIL import Image

#Открывает картинку и сохраняет её в 2 переменные
image1 = image2 = Image.open("diff.png")

#Ширина линии между картинками
lineWidth = 29
#Максимальная допустимая разница между пикселями
maxDiff = 50

#Обрезает картинку попалам
image1 = image1.crop((0, 0, image1.width/2 - lineWidth/2, image1.height))
image2 = image2.crop((image2.width/2 + lineWidth/2, 0, image2.width, image1.height))

#Создает пустую картинку с празрачностью и с такими-же размерами как и оригинал
resultImage = Image.new("RGBA", (image1.width, image1.height))

#Загружает все пиксели
image1Pixels = image1.load()
image2Pixels = image2.load()
resultPixels = resultImage.load()

#Проходит по всем пикселям
for x in range(resultImage.width):
    for y in range(resultImage.height):

        #Вычесляет среднее значение цвета пикселя на первой картинке и на второй
        avg1 = sum(image1Pixels[x, y]) / len(image1Pixels[x, y])
        avg2 = sum(image2Pixels[x, y]) / len(image2Pixels[x, y])

        #Если разница между двумя средними больше чем домустимая, то на путой картинке задать пиксель зеленым
        if(abs(avg1 - avg2) > maxDiff):
            resultPixels[x,y] = (0, 255, 0, 255)


#Наложить результат на первую картинку чтобы увидеть отличия
Image.Image.paste(image1, resultImage, mask=resultImage)

#Открыть картинки
#resultImage.show()
image1.show()
#image2.show()