#Из библиотеки tkinter импортировать все (что библиотека отметила как "все")
from tkinter import *
#Из библиотеки tkinter импортировать ttk (themed tkinter) для красоты
from tkinter import ttk
#Из библиотеки tkinter импортировать messagebox для отоброжения окон с сообщениями и ошибками
from tkinter import messagebox

#Создать объект окна, задать название и размеры
window = Tk()
window.title("horrible ui")
window.geometry("700x700")

#Создать текст, кнопку, и текстовое поле и настроить их
label = ttk.Label(window, text = "Hello, welcome to my program :)")
button = ttk.Button(
    window,
    text = "DO NOT PRESS",
    command= lambda : print(textField.get()))
textField = ttk.Entry(window)

#Сложить компоненты друг за другом
label.pack()
button.pack()
#Задать конкретные координаты где отоброзить компонент
textField.place(x = 300, y = 200)

#Показать 2 сообщения
messagebox.showerror(title=":))))))",message = "Hello this is a bad ui")
messagebox.askquestion(title="question",message = "Is that ok?")

#Запустить основной цикл окна чтобы оно обробатовала все интеракции
window.mainloop()