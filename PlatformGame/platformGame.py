import pygame
import random
from PlatformGame.gameSprites import *

pygame.init()
pygame.font.init()
pygame.mixer.init()

scoreFont = pygame.font.SysFont("Arial", 40, bold=True)

pygame.mixer.music.load("music.mp3")
pygame.mixer.music.set_volume(0.2)
pygame.mixer.music.play()

#testSound = pygame.mixer.Sound("music.mp3")

screen = pygame.display.set_mode( (WIDTH, HEIGHT) )
clock = pygame.time.Clock()

def restart():
    global playing, isGameOver, ball, lastPlatformPos, platforms, score

    score = 0
    playing = True
    isGameOver = False

    ball = Ball((200, 200), PLAYERIMAGE, (80, 80))

    platforms = []
    for i in range(PLATFORMCOUNT):
        randx = random.randint(0 - PLATFORMSIZE[0], WIDTH/2 - PLATFORMSIZE[0])
        platforms.append(Platform((randx, i * SPACING), PLATFORMIMAGE, PLATFORMSIZE))
        platforms.append(Platform((randx + HOLESIZE + PLATFORMSIZE[0], i * SPACING), PLATFORMIMAGE, PLATFORMSIZE))

    lastPlatformPos = platforms[-1].rect.y

def events():
    global playing
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            playing = False

def logic():
    global score, playing, isGameOver, platformSpeed

    keys = pygame.key.get_pressed()

    if not isGameOver:
        if keys[pygame.K_d] and ball.rect.right < WIDTH + 20:
            ball.rect.x += ball.speed
        if keys[pygame.K_a] and ball.rect.left > -20:
            ball.rect.x -= ball.speed


        for platform in platforms:
            platform.Rise(platformSpeed)

        collided = pygame.sprite.spritecollide(ball, platforms, False)

        if collided and ball.rect.bottom < collided[0].rect.bottom:
            ball.rect.bottom = collided[0].rect.top

        else:
            ball.FallDown()
            score += 1
    else:
        if keys[pygame.K_r]:
            restart()

    #Za predelq
    if ball.rect.bottom <= -10 or ball.rect.top >= HEIGHT + 10:
        isGameOver = True


    platformSpeed = 2 + score / 100
    ball.speed = 10 + platformSpeed

def draw():
    global isGameOver
    screen.fill(BACKGROUNDCOLOR)

    screen.blit(ball.image, (ball.rect.x, ball.rect.y))
    for platform in platforms:
        screen.blit(platform.image, (platform.rect.x, platform.rect.y))

    #UI
    scoreText = scoreFont.render("SCORE: " + str(score), True, (255, 30,100))
    screen.blit(scoreText, (20,20))

    if(isGameOver):
        gotext = scoreFont.render("GAME OVER", True, (255, 255, 255), (0,0,0))
        restartText = scoreFont.render("Press 'R' to restart", True, (255, 255, 255), (0,0,0))
        screen.blit(gotext, (WIDTH/2 - 150,HEIGHT/2 - 100))
        screen.blit(restartText, (WIDTH/2 - 200,HEIGHT/2 - 50))

    pygame.display.flip()

#MAIN LOOP
restart()
while playing:
    clock.tick(FPS)

    events()
    logic()
    draw()
