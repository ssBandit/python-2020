import pygame
from PlatformGame.gameVars import *
import random

class Ball(pygame.sprite.Sprite):
    def __init__(self, pos, imagefile, size, speed = 10):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(imagefile)
        self.image = pygame.transform.scale(self.image, size)
        self.image.set_colorkey((255,255,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.speed = speed

    def FallDown(self):
        self.rect.y += GRAVITY

class Platform(pygame.sprite.Sprite):
    def __init__(self, pos, imagefile, size):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(imagefile)
        self.image = pygame.transform.scale(self.image, size)
        self.image.set_colorkey((255,255,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]

    def Rise(self, speed = 10):

        self.rect.y -= speed
        if(self.rect.y < 0 - self.rect.height):
            self.rect.y = lastPlatformPos + SPACING * PLATFORMCOUNT
