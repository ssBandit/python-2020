from flask import Flask, render_template, request, send_file
import random
import os


app = Flask(__name__)

listnotes = []

@app.route('/')
def home():
    return render_template('index.html', asdf = random.randint(0, 1000))

@app.route('/secret')
def secret():
    restultText = ""
    for i in range(500):
        restultText += "you found a secret page, tell nobody<br>"
    return restultText

@app.route('/notes', methods=["GET", "POST"])
def notes():
    file = open('notes.txt', 'r')
    listnotes = list(filter(None, file.read().split('\n')))
    file.close()

    if request.method == "POST":
        data = request.form['note']
        listnotes.append(data)
        open('notes.txt', 'a').write(data + "\n")

    return render_template('notes.html', displaynotes = listnotes, noteslen = len(listnotes))

@app.route('/files', methods=["GET", "POST"])
def files():

    if request.method == "POST":
        file = request.files["file"]
        file.save(os.path.join(app.root_path, 'uploads', file.filename))

    fileslist = os.listdir('uploads')
    return render_template('files.html', filenames = fileslist, filecount = len(fileslist))

@app.route("/files/<path:filename>")
def download(filename):
    file = os.path.join(app.root_path, 'uploads', filename)
    return send_file(file)

@app.errorhandler(404)
def notfound(error):
    return "snooPING AS usual i see"


app.run(debug=True, host="192.168.31.57", port=80)