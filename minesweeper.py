from tkinter import *
import random

'''
-2      = нажатая клетка
-1      = мина
0       = пустое место
1 - 8   = мины по соседству
'''

#Количество клеток на поле
WIDTH = 10
HEIGHT = 10

#Процент генерации мин
MINECHANCE = 20

window = Tk()
#Задать размеры окна соответственно с количеством клеток
window.geometry(str(40 * WIDTH) +"x"+  str(43 * HEIGHT))
#Запретить менять размеры окна (по горизонтали и вертикали)
window.resizable(False, False)

#Двухмерный список кнопок (список списков)
buttons = []
#Двухмерный список данных поля
minefield = []

#Проверить соседий и "нажать" на них
def CheckNeighbors(x,y):

    #vertical
    if(x > 0):
        if(minefield[x-1][y] >= 0):
            buttons[x-1][y].invoke()
    if(x < WIDTH-1):
        if(minefield[x+1][y] >= 0):
            buttons[x+1][y].invoke()
    #horizontal
    if(y > 0):
        if(minefield[x][y-1] >= 0):
            buttons[x][y-1].invoke()
    if(y < HEIGHT-1):
        if(minefield[x][y+1] >= 0):
            buttons[x][y+1].invoke()

    #diagonals
    if(x < WIDTH-1 and y > 0):
        if(minefield[x+1][y-1] >= 0):
            buttons[x+1][y-1].invoke()

    if(x > 0 and y > 0):
        if(minefield[x-1][y-1] >= 0):
            buttons[x-1][y-1].invoke()

    if(x > 0 and y < HEIGHT -1):
        if(minefield[x-1][y+1] >= 0):
            buttons[x-1][y+1].invoke()

    if(x < WIDTH -1 and y < HEIGHT -1):
        if(minefield[x+1][y+1] >= 0):
            buttons[x+1][y+1].invoke()

#Пощитать мины по соседству
def CountMines(x,y):

    #vertical
    if(x > 0):
        if(minefield[x-1][y] == -1):
            minefield[x][y] += 1
    if(x < WIDTH-1):
        if(minefield[x+1][y] == -1):
            minefield[x][y] += 1
    #horizontal
    if(y > 0):
        if(minefield[x][y-1] == -1):
            minefield[x][y] += 1
    if(y < HEIGHT-1):
        if(minefield[x][y+1] == -1):
            minefield[x][y] += 1

    #diagonals
    if(x < WIDTH-1 and y > 0):
        if(minefield[x+1][y-1] == -1):
            minefield[x][y] += 1

    if(x > 0 and y > 0):
        if(minefield[x-1][y-1] == -1):
            minefield[x][y] += 1

    if(x > 0 and y < HEIGHT -1):
        if(minefield[x-1][y+1] == -1):
            minefield[x][y] += 1

    if(x < WIDTH -1 and y < HEIGHT -1):
        if(minefield[x+1][y+1] == -1):
            minefield[x][y] += 1

#При нажатии поменять цвет кнопки на данной позиции
def OnButtonClick(x,y):
    if(minefield[x][y] == 0):
        buttons[x][y].config(bg = "green")
        minefield[x][y] = -2
        CheckNeighbors(x, y)
    elif(minefield[x][y] == -1):
        buttons[x][y].config(bg = "red")
    elif(minefield[x][y] > 0):
        buttons[x][y].config(text = minefield[x][y])



#Создать сетку кнопок получив количество в ширину и высоту
def CreateButtons(width, height):
    for x in range(width):
        #На каждой строчке добавить новый список в список (чтобы сделать 2-х мерным)
        buttons.append([])
        minefield.append([])
        for y in range(height):
            #Создать кнопку и привезать ей функцию которая получает координаты кнопки
            button = Button(window, width = 4, height = 2, command = lambda x=x, y=y: OnButtonClick(x,y))
            #Поставить кнопку на сетку на ряд и колонну
            button.grid(row = x, column = y)
            #Добавить в список на ряду Х эту кнопку
            buttons[x].append(button)

            #Записать 0 на пустых клетках
            minefield[x].append(0)
            #Если рандомный шанс выподает то поставить туда мину (-1)
            if(random.randint(0, 100) < MINECHANCE):
                minefield[x][y] = -1

    #Пройтись еще рас по полю и проверить соседние мины для каждой
    for x in range(width):
        for y in range(height):
            CountMines(x,y)

#Вызвать функцию передавая высоту и ширину
CreateButtons(HEIGHT, WIDTH)

window.mainloop()
