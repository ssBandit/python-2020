import tkinter as tk

#Logic
clicks = 0
autoclickers = 0
prices = [10, 500, 10000]

def click():
    global clicks
    clicks += 1
    clickButton.config(text = str(clicks))

def buy(price, clickers):
    global clicks
    global autoclickers

    if(clicks < price): return

    clicks -= price
    autoclickers += clickers
    clickButton.config(text=str(clicks))
    clickerCount.config(text = "Autoclickers: " + str(autoclickers))

def autoclick():
    global clicks
    clicks += autoclickers
    clickButton.config(text=str(clicks))

    window.after(500, autoclick)

#UI
window = tk.Tk()
window.minsize(600,600)
title = tk.Label(text = "COOL CLICKER",
                 font = "Impact 50",
                 fg = "blue")

clickButton = tk.Button(text = "CLICK ME!",
                        font = "Arial 20",
                        bg = "red",
                        fg = "white",
                        command = click)

autoclickX1 = tk.Button(text = "Buy 1 autoclicker\nPrice: " + str(prices[0]) + " clicks",
                        command = lambda: buy(prices[0], 1))

autoclickX10 = tk.Button(text = "Buy 10 autoclicker\nPrice: " + str(prices[1]) + " clicks",
                        command = lambda: buy(prices[1], 10))

clickerCount = tk.Label(text = "Autoclickers: " + str(autoclickers))

title.pack()
clickButton.pack(pady = 30)
clickerCount.pack()
autoclickX1.pack(side = "left", padx = 20)
autoclickX10.pack(side = "left", padx = 20)


autoclick()

window.mainloop()