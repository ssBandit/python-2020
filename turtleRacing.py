import turtle
import random

#Параметры поля
LINE_LENGTH = 700
LINE_WIDTH = 50
TURTLE_COUNT = 5
TURTLE_COLORS = ["red", "green", "yellow", "magenta", "purple", "OrangeRed", "blue", "cyan", "DodgerBlue"]


#Создать черепаху которая сетку рисует
liner = turtle.Turtle()
liner.speed(0)

#Функция чтобы сделать сетку
def makeGrid():

    liner.penup()
    liner.goto(-300, 300)
    liner.pendown()

    for i in range(TURTLE_COUNT):
        liner.forward(LINE_LENGTH)
        liner.backward(LINE_LENGTH)
        liner.goto(-300, liner.position()[1]-LINE_WIDTH)
    #Одна дополнительная линия
    liner.forward(LINE_LENGTH)

#Вызов функции создания сетки
makeGrid()

#Создать пустой список черепах
turtles = []
#Пройти по нужному количеству черепах
for i in range(TURTLE_COUNT):
    #Создать новую и задать ей рандомный цвет
    turtOne = turtle.Turtle()
    turtOne.color(random.choice(TURTLE_COLORS))
    turtOne.shape("turtle")

    turtOne.penup()
    #Пойти на стартувую позицию
    turtOne.goto(-300,300-(LINE_WIDTH/2) - LINE_WIDTH * i)
    turtles.append(turtOne)

isFinished = False
while(not isFinished):
    #Пройтись по каждой черепахе
    for turt in turtles:
        #Сдвинуть вперед на рандомное количество
        turt.forward(random.randint(5, 15))
        #Если дошла до конца то написать текст и выйти из обеих циклов
        if(turt.position()[0] >= liner.position()[0]):
            turt.write("I WIN, BYE BYE")
            isFinished = True
            break

turtle.done()