import turtle
from threading import Thread

#Переменные для размеров окна
WIDTH = 200
HEIGHT = 200

#Задать размеры окна
turtle.setup(WIDTH, HEIGHT)

#Создать черепаху и настроить её
anton = turtle.Turtle()
anton.speed(0)
anton.pensize(8)
anton.shapesize(2)

#Функции для поворачивания
def up():
    anton.setheading(90)
    anton.forward(20)

def down():
    anton.setheading(270)
    anton.forward(20)

def left():
    anton.setheading(180)
    anton.forward(20)

def right():
    anton.setheading(0)
    anton.forward(20)

#Функция для постоянного движения вперед
def go():
    while(True):
        anton.forward(4)

        #Проверить если заходит за края по Х координате
        if(abs(anton.position()[0]) > WIDTH/2):
            anton.penup()
            #Если зашел за край то телепортировать на минусавую координату по Х
            anton.setpos(-anton.position()[0], anton.position()[1])
            anton.pendown()

        # Проверить если заходит за края по Y координате
        if (abs(anton.position()[1]) > HEIGHT/2):
            anton.penup()
            #Если зашел за край то телепортировать на минусавую координату по Y
            anton.setpos(anton.position()[0], -anton.position()[1])
            anton.pendown()

        #turtle.update()

#Подписатся на эвенты нажатия кнопок
turtle.onkey(up, "w")
turtle.onkey(down, "s")
turtle.onkey(left, "a")
turtle.onkey(right, "d")

#Создать новый тред (поток) и задать цель
goThread = Thread(target=go)

#Запустить тред
goThread.start()

#Слушать эвенты нажатия
turtle.listen()

#Циклить бесконечно так как картинка никогда не будет дорисованна
turtle.mainloop()