import tkinter as tk
from pynput import keyboard
from pynput import mouse
from pynput.keyboard import Controller, Key, Listener

import clipboard
import time

kb = Controller()

def doshortcut():
    kb.release('k')
    kb.release(keyboard.Key.ctrl)
    kb.release(keyboard.Key.alt)

    #выделить строчку
    kb.press(keyboard.Key.shift)
    kb.press(keyboard.Key.home)

    kb.release(keyboard.Key.shift)
    kb.release(keyboard.Key.home)

    #скопировать её
    kb.press(keyboard.Key.ctrl)
    kb.press('c')

    kb.release('c')
    kb.release(keyboard.Key.ctrl)
    time.sleep(0.1)
    #перевести
    print(clipboard.paste())
    translated = translate(clipboard.paste())
    print(translated)
    clipboard.copy(translated)

    #вставить
    kb.press(keyboard.Key.ctrl)
    kb.press('v')

    kb.release('v')
    kb.release(keyboard.Key.ctrl)

# listen = Listener(on_press = doshortcut)
# listen.start()

hotkey = keyboard.GlobalHotKeys({"<ctrl>+<alt>+k": doshortcut})
hotkey.start()

# translation
eng = "qwertyuiop[]asdfghjkl;'zxcvbnm,./`"
rus = "йцукенгшщзхъфывапролджэячсмитьбю.ё"

zipped = zip(eng, rus)
eng2rus = dict(zipped)

zipped = zip(rus, eng)
rus2eng = dict(zipped)


def translateUI(toRus=True):
    text = textField.get().lower()

    result = translate(text, toRus)

    textField.delete(0, len(text))
    textField.insert(0, result)


def translate(textToTranslate, toRus=True):

    toRus = textToTranslate[0] in eng2rus

    if toRus:
        dictToUse = eng2rus
    else:
        dictToUse = rus2eng

    result = ""
    for letter in textToTranslate:
        if letter in dictToUse:
            result += dictToUse[letter]
        else:
            result += letter

    return result


# UI
window = tk.Tk()
window.title("translator")
textField = tk.Entry(width=80)
translateButton = tk.Button(text="Translate to rus", command=lambda: translateUI(toRus=True))
translateButton2 = tk.Button(text="Translate to eng", command=lambda: translateUI(toRus=False))

textField.pack()
translateButton.pack(side="left")
translateButton2.pack(side="right")

window.mainloop()
